![compatibility](https://img.shields.io/badge/compatibility-Nuke%2FNukeX%2011%2C12-green)

***NUKE FILE PATH RECOVER TOOL***
This tool finds and replaces all invlalid paths within the Node Graph
*********************************************************************
Last modified: 16/01/2020
Version 1.0
*********************************************************************
Nuke compability test: Nuke/NukeX 11,12
*********************************************************************

HOW TO INSTALL AND RUN:
**********************************************************************************************************************
1. Navigate to you /.nuke directory (this should be automatically created after Nuke startup)

2. If the original menu.py file is not empty copy the code from this menu.py

3. Copy all files to your /.nuke directory

4. Open Nuke

5. A new icon appears on the left Nuke Toolbar panel 

6. Click it to run the tool or press "alt+R"

7. Click Search and Replace or press "Enter"

8. Specify the  new directory where the files from Node Graph can be found (less specific takes more time to compute)
***********************************************************************************************************************

CREATED BY:
****************************
suska.filip@gmail.com
lukas.danko1@gmail.com
****************************



