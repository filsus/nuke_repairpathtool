import nuke
import os.path
try:
    from PySide import QtGui, QtCore
except ImportError:
    from PySide2 import QtCore
    from PySide2 import QtWidgets as QtGui
    from PySide2 import QtGui as QtG
import re

#Loading Widget Window
class ProgressBarDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        
        self.setLayout(QtGui.QVBoxLayout())
        self.setWindowTitle("Repairing Invalid Paths")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.label = QtGui.QLabel("Please wait...", self)
        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setRange(0,0)
        self.label.setObjectName("label")
        self.layout().addWidget(self.label) 
        self.layout().addWidget(self.progressBar) 

    def closeWindow(self):
        self.close()
    
    def openWindow(self):
        self.open()

#Main Window
class SearchAndReplace(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(SearchAndReplace, self).__init__(parent)
        self.allInvalidPaths = []
        self.nukePathSeparator = "/"
        self.allowedFormats = []
        self.firstLetters = []
        self.progressBar = ProgressBarDialog()
          
    #---------------------------------------------------------------------
    # GUI
    #---------------------------------------------------------------------
      
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setupUi(self)        

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(300, 280)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.buttonSearchReplace = QtGui.QPushButton(self.centralwidget)
        self.buttonSearchReplace.setGeometry(QtCore.QRect(10, 230, 140, 30))
        font = QtG.QFont()
        font.setPointSize(8)
        font.setWeight(75)
        font.setBold(True)
        self.buttonSearchReplace.setFont(font)
        self.buttonSearchReplace.setObjectName("buttonSearchReplace")
        self.buttonUpdateWindow = QtGui.QPushButton(self.centralwidget)
        self.buttonUpdateWindow.setGeometry(QtCore.QRect(170, 230, 120, 30))
        font = QtG.QFont()
        font.setPointSize(8)
        font.setWeight(75)
        font.setBold(False)
        self.buttonUpdateWindow.setFont(font)
        self.buttonUpdateWindow.setObjectName("buttonUpdateWindow")
        self.listWidget = QtGui.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 30, 281, 191))
        self.listWidget.setObjectName("listWidget")
        self.label1 = QtGui.QLabel(self.centralwidget)
        self.label1.setGeometry(QtCore.QRect(10, 10, 91, 16))
        self.label1.setObjectName("label1")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        #button1    
        self.buttonUpdateWindow.clicked.connect(self.updateWindow) 
        #buttonSearchReplace
        self.buttonSearchReplace.clicked.connect(self.searchAndReplace)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None))
        self.buttonSearchReplace.setText(QtGui.QApplication.translate("MainWindow", "Search and Replace", None))
        self.buttonSearchReplace.setShortcut(QtGui.QApplication.translate("MainWindow", "Return", None))
        self.buttonUpdateWindow.setText(QtGui.QApplication.translate("MainWindow", "UPDATE WINDOW", None))
        self.label1.setText(QtGui.QApplication.translate("MainWindow", "Invalid paths:", None))

    #--------------------------------------------------------------
    # BUTTONS ACTION
    #--------------------------------------------------------------

    def updateWindow(self): #type: () -> None
        self.close()
        self.allowedFormats = []
        self.firstLetters = []
        self.allInvalidPaths = self.getAllInvalidPaths()
        self.createList()
        self.show()
    
    def searchAndReplace(self): #type: () -> None
        self.close()
        userSelectPath = nuke.getFilename("Get Folder Path")
        self.progressBar.openWindow()
        QtGui.QApplication.processEvents()
        for invalidPath in self.allInvalidPaths:
            invalidPathArray = invalidPath.split(self.nukePathSeparator)
            nodesForInvalidPath = self.allInvalidPaths.get(invalidPath)
            QtGui.QApplication.processEvents()
            self.recursive(userSelectPath, nodesForInvalidPath, invalidPathArray)
        
        self.allInvalidPaths = self.getAllInvalidPaths()
        if(len(self.allInvalidPaths) > 0):
            self.findPathOsWalk(userSelectPath)
        self.progressBar.closeWindow() 
        self.updateWindow()

    #--------------------------------------------------------------
    # GUI Methods
    #--------------------------------------------------------------

    #add string represenation of invalidit to GUI listWidget
    def createList(self): #type: () -> void
        listWidget = self.listWidget
        listWidget.clear()
        for filePath in self.allInvalidPaths:
            item = QtGui.QListWidgetItem("%s" % filePath)
            listWidget.addItem(item)

    #--------------------------------------------------------------
    # CORE LOGIC
    #--------------------------------------------------------------

    #find and map all corrupted paths
    def getAllInvalidPaths(self): #type: () -> typing.Dict[str, typing.List[nuke.Node]]
        invalidPath = dict()
        for node in nuke.allNodes(recurseGroups=True):
            if (node.Class() == "Read" or node.Class() == "ReadGeo" or node.Class() == "Camera2") and node.treeHasError():
                path = node.knob("file").value()
                self.fillFirstLetterAndAllowedExt(path)
                if path not in invalidPath:
                    invalidPath[path] = [node]
                else:
                    invalidPath[path].append(node)            
        return invalidPath
    
    # find path with shorting path alogrithm
    def recursive(self, userSelectPath, nodes, invalidPathArray): #type: (str, typing.List[nuke.Nodes], str) -> void
        invalidPath = (self.nukePathSeparator).join(invalidPathArray)
        finalPath = userSelectPath + invalidPath
        if(not self.isValidPath(nodes[0], finalPath)):
            #use recursive search with a common folder method
            if len(invalidPathArray) > 1:
                invalidPathArray.pop(0)
                self.recursive(userSelectPath, nodes, invalidPathArray)         
        else:
            self.setPathToNodes(nodes, finalPath)

    #find path in remaped folders by os.walk
    def findPathOsWalk(self, userSelectPath): #type: (str) -> void
        remapedFolders = self.getRemapedAllPossibleFolders(userSelectPath)
        for path, value in remapedFolders.items():
            QtGui.QApplication.processEvents()
            pathFixed = False
            firstFile = value.get("firstItem")
            for invalidPath in self.allInvalidPaths:
                if(self.compareFiles(invalidPath, path, firstFile)):
                    pathFixed = True
                    break
            if(pathFixed):
                continue
            for file in value.get("generator"):
                print(file)
                for invalidPath in self.allInvalidPaths:
                    if(self.compareFiles(invalidPath, path, file)):
                        break                  

    #--------------------------------------------------------------
    # Helpers
    #--------------------------------------------------------------

    #compare if files are identical
    def compareFiles(self, invalidPath, path, file): #type: (str, str, str) -> bool
        result = False
        invalidFileHelper = FileHelper(self.getFilename(invalidPath))
        paddingFormat = self.getPaddingFormat(invalidFileHelper.getName())
        invalidFileHelper.setPaddingIndex(paddingFormat)
        fileHelper = FileHelper(file)
        fileHelper.setPaddingIndex(paddingFormat)
        if (invalidFileHelper.getEscapedName() == fileHelper.getEscapedName() and invalidFileHelper.getExtension() == fileHelper.getExtension()):
            nodes = self.allInvalidPaths.get(invalidPath)
            finalPath = self.nukePathSeparator.join(path.split(os.path.sep))
            self.setPathToNodes(nodes, finalPath + self.nukePathSeparator + invalidFileHelper.getFullFilename())
            result = True
        return result  

    # remap all possible folders via os.Walk
    def getRemapedAllPossibleFolders(self, userSelectPath): #type: (str) -> Dict[str, list[str]]
        mapFolders = dict()
        for path, dirs, files in os.walk(userSelectPath):
            QtGui.QApplication.processEvents()
            if (len(files) > 0):
                remapedFiles = (file for file in files if self.isAllowedFile(file))
                try: 
                    firstItem = next(remapedFiles)
                    mapFolders[path] = {"firstItem": firstItem, "generator": remapedFiles}
                except StopIteration:
                    continue
        return mapFolders

    #return filename from path
    def getFilename(self, path): #type: (str) -> string
        return path.split(self.nukePathSeparator)[-1]

    #set same path to all nodes in array of Nodes 
    def setPathToNodes(self, nodes, finalPath): #type: (typing.List[nuke.Node], str) -> None
        for node in nodes:
            node.knob("file").setValue(finalPath)

    #fill list of first letters of invalid files and allowed formats
    def fillFirstLetterAndAllowedExt(self, path): #type: (str) -> void
        fileHelper = FileHelper(self.getFilename(path))
        if (fileHelper.getFullFilename()[0] not in self.firstLetters):
            self.firstLetters.append(fileHelper.getFullFilename()[0])
        if (fileHelper.getExtension() not in self.allowedFormats):
            self.allowedFormats.append(fileHelper.getExtension())

    #check if file exist
    def isValidPath(self, node, path): #type: (nuke.Node, str) -> bool
        result = True
        originalVaue = node.knob("file").value()
        node.knob("file").setValue(path)
        if (node.treeHasError()):
            node.knob("file").setValue(originalVaue)
            result = False
        return result

    #return start index of padding and number how long padding is
    def getPaddingFormat(self, filename): #type: (str) -> Set[int, int]
        regex =  re.search(r'\%0[0-9]+d', filename)
        if(regex != None):
            index = regex.start()
            return {"index": index, "digits": int(filename[index + 2]) }
        return {"index": None, "digits": None }

    #check if file has allowed ext and first letter
    def isAllowedFile(self, filename): #type: (str) -> bool
        fileHelper = FileHelper(filename)
        if(filename[0] in self.firstLetters and fileHelper.getExtension() in self.allowedFormats):
            return True
        return False

    def loadOnStart(self):
        self.updateWindow()
        self.show()

                
class FileHelper:
    def __init__(self, fileNameWithExtesion):
        self.fileNameWithExtesion =  fileNameWithExtesion
        self.startPaddingIndex = None
        self.numberOfdigits = None

    #return file extenstion
    def getExtension(self): #type: () -> str
        return  self.fileNameWithExtesion.split(".")[-1]
    
    #return name without extension
    def getName(self): #type: () -> str
        return ".".join(self.fileNameWithExtesion.split(".")[:-1])
    
    #return name without paddind
    def getEscapedName(self): #type: () -> str
        name = self.getName()
        if (self.startPaddingIndex != None and self.numberOfdigits != None):
            startName = name[:self.startPaddingIndex]
            endName = name[self.startPaddingIndex + self.numberOfdigits:len(name)]
            return startName + endName
        return name
    
    #return fulname
    def getFullFilename(self): #type: () -> str
        return self.fileNameWithExtesion
    
    #set padding index
    def setPaddingIndex(self, paddingFormat): #type: (Set[int, int]) -> void
        self.startPaddingIndex = paddingFormat.get("index")
        self.numberOfdigits = paddingFormat.get("digits")
